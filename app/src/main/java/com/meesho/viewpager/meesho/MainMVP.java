package com.meesho.viewpager.meesho;

import com.meesho.viewpager.meesho.model.DogModel;

/**
 * Created by brainnr on 10/23/17.
 */

public interface MainMVP {

    interface RequiredPresenterOps {
        void dogAdded(DogModel dogModel);

        void dogRemoved(DogModel dogModel);
    }

    interface RequiredViewOps {
        void startAutoScroll();

        void stopAutoScroll();

        void toggleDeleteBtn(int visibility);

        void toggleAddBtn(int visibility);

        void showToast(String message);

        void addViewPagerItem(DogModel dogModel);

        void removeViewPagerItem(DogModel dogModel);
    }

    interface PresenterOps {
        void addNewDog();

        void deleteDogModel();

        void onConfigurationChanged(RequiredViewOps view);

        void onDestroy(boolean isChangingConfig);
    }

    interface ModelOps {
        void insertDog(DogModel dogModel);

        void removeDog(DogModel dogModel);

        void onDestroy();

    }
}
