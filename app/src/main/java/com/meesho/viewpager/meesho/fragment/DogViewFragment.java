package com.meesho.viewpager.meesho.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.meesho.viewpager.meesho.R;
import com.meesho.viewpager.meesho.model.DogModel;

public class DogViewFragment extends Fragment {

    private static final String ARG_MODEL_DOG = "MODEL_DOG";

    private DogModel dogModel;
    private SimpleDraweeView dogImageView;

    public DogViewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param dogModel Parameter 1.
     * @return A new instance of fragment DogViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DogViewFragment newInstance(DogModel dogModel) {
        DogViewFragment fragment = new DogViewFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_MODEL_DOG, dogModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dogModel = (DogModel) getArguments().getSerializable(ARG_MODEL_DOG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dog_view, container, false);
        TextView viewIdx = (TextView) view.findViewById(R.id.view_idx);
        dogImageView = (SimpleDraweeView) view.findViewById(R.id.dog_image_view);

        viewIdx.setText(getString(R.string.view_idx, dogModel.getIdx()));
        dogImageView.setImageURI(dogModel.getUrl());
        return view;
    }

    public DogModel getDogModel() {
        return dogModel;
    }
}
