package com.meesho.viewpager.meesho.model;

import com.meesho.viewpager.meesho.MainMVP;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brainnr on 10/23/17.
 */

public class MainModel implements MainMVP.ModelOps {

    private MainMVP.RequiredPresenterOps requiredPresenterOps;
    private List<DogModel> dogModelList;

    public MainModel(MainMVP.RequiredPresenterOps requiredPresenterOps) {
        this.requiredPresenterOps = requiredPresenterOps;
        dogModelList = new ArrayList<>();
    }

    @Override
    public void insertDog(DogModel dogModel) {
        dogModelList.add(dogModel);
        // Write the dogModelList to Storage for re-usage
        requiredPresenterOps.dogAdded(dogModel);
    }

    @Override
    public void removeDog(DogModel dogModel) {
        // Write the dogModelList to Storage for re-usage
        dogModelList.remove(dogModel);
        requiredPresenterOps.dogRemoved(dogModel);
    }

    @Override
    public void onDestroy() {

    }
}
