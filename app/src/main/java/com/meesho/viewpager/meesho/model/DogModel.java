package com.meesho.viewpager.meesho.model;

import java.io.Serializable;

/**
 * Created by brainnr on 10/23/17.
 */

public class DogModel implements Serializable {

    public static final String[] dogList = new String[]{
            "https://i.imgur.com/C9Wz6G8.jpg",
            "https://i.imgur.com/bdh4Qpn.jpg",
            "https://i.imgur.com/UMKNd1b.jpg",
            "https://i.imgur.com/mG7rmUW.jpg",
            "https://i.imgur.com/IGGBxys.jpg",
            "https://i.imgur.com/MF6hysE.jpg",
            "https://i.imgur.com/wC3u4rM.jpg",
            "https://i.imgur.com/MPwlRPe.jpg",
            "https://i.imgur.com/TvGOeJ6.jpg",
            "https://i.imgur.com/ud8DGlY.jpg"
    };

    private int idx;
    private String url;

    public int getIdx() {
        return idx;
    }

    public DogModel(int idx) {
        this.idx = idx;
        this.url = dogList[idx];
    }

    public String getUrl() {
        return url;
    }
}
