package com.meesho.viewpager.meesho;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.meesho.viewpager.meesho.adapter.CustomFragmentPagerAdapter;
import com.meesho.viewpager.meesho.model.DogModel;
import com.meesho.viewpager.meesho.presenter.MainPresenter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainMVP.RequiredViewOps {

    private final String TAG = getClass().getSimpleName();

    private StateMaintainer stateMaintainer = new StateMaintainer(this.getSupportFragmentManager(), TAG);
    private MainMVP.PresenterOps presenterOps;

    //ViewVariables
    private Button btnAddItem;
    private Button btnRemoveItem;
    private ViewPager mainViewPager;

    //DataVariable
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startMVPOps();
        Fresco.initialize(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnAddItem = (Button) findViewById(R.id.btn_add);
        btnRemoveItem = (Button) findViewById(R.id.btn_remove);
        mainViewPager = (ViewPager) findViewById(R.id.main_viewpager);

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenterOps.addNewDog();
            }
        });

        btnRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenterOps.deleteDogModel();
            }
        });

        mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int numOfItems = mainViewPager.getAdapter().getCount();
                if (numOfItems > 2) {
                    Log.d(TAG, "position:" + position + ", numOfItems:" + numOfItems);
                    if (position == numOfItems - 1) {
                        mainViewPager.setCurrentItem(1, false);
                    } else if (position == 0) {
                        mainViewPager.setCurrentItem(numOfItems - 2, false);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        CustomFragmentPagerAdapter adapter = new CustomFragmentPagerAdapter(getSupportFragmentManager(), new ArrayList<DogModel>());
        mainViewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startMVPOps() {
        try {
            if (stateMaintainer.firstTimeIn()) {
                initialize();
            } else {
                reinitialize(this);
            }
        } catch (InstantiationError e) {
            throw new RuntimeException(e);
        }
    }

    public void initialize() {
        presenterOps = new MainPresenter(this);
        stateMaintainer.put(presenterOps.getClass().getSimpleName(), presenterOps);
    }


    public void reinitialize(MainMVP.RequiredViewOps requiredViewOps) {
        presenterOps = stateMaintainer.get(presenterOps.getClass().getSimpleName());

        if (presenterOps == null) {
            presenterOps = new MainPresenter(requiredViewOps);
        } else {
            presenterOps.onConfigurationChanged(requiredViewOps);
        }
    }

    @Override
    public void startAutoScroll() {
        Log.d(TAG, "startAutoScroll");
        countDownTimer = new CountDownTimer(Long.MAX_VALUE, 4000) {
            @Override
            public void onTick(long l) {
                int idx = mainViewPager.getCurrentItem();
                int count = mainViewPager.getAdapter().getCount();

                if (idx == (count - 2)) {
                    mainViewPager.setCurrentItem(1);
                } else {
                    mainViewPager.setCurrentItem(idx + 1);
                }
            }

            @Override
            public void onFinish() {

            }
        };
        countDownTimer.start();
    }

    @Override
    public void stopAutoScroll() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @Override
    public void toggleDeleteBtn(int visibility) {
        btnRemoveItem.setVisibility(visibility);
    }

    @Override
    public void toggleAddBtn(int visibility) {
        btnAddItem.setVisibility(visibility);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void addViewPagerItem(DogModel dogModel) {
        if (mainViewPager.getAdapter() == null) {
            List<DogModel> dogModelList = new ArrayList<>();
            dogModelList.add(dogModel);
            CustomFragmentPagerAdapter adapter = new CustomFragmentPagerAdapter(getSupportFragmentManager(), dogModelList);
            mainViewPager.setAdapter(adapter);
        } else {
            CustomFragmentPagerAdapter adapter = (CustomFragmentPagerAdapter) mainViewPager.getAdapter();
            adapter.addDogModel(dogModel);
            int count = adapter.getCount();
            if (count > 2) {
                mainViewPager.setCurrentItem(count - 2, true);
            }
        }
    }

    @Override
    public void removeViewPagerItem(DogModel dogModel) {
        if (mainViewPager.getAdapter() != null) {
            CustomFragmentPagerAdapter adapter = (CustomFragmentPagerAdapter) mainViewPager.getAdapter();
            adapter.removeDogModel(dogModel);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }
}
