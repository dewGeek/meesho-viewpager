package com.meesho.viewpager.meesho.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.meesho.viewpager.meesho.fragment.DogViewFragment;
import com.meesho.viewpager.meesho.model.DogModel;

import java.util.List;

/**
 * Created by DEWGEEK on 3/8/16.
 */
public class CustomFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private final String TAG = getClass().getSimpleName();

    private List<DogModel> dogModelList;

    public CustomFragmentPagerAdapter(FragmentManager fm, List<DogModel> dogModelList) {
        super(fm);
        this.dogModelList = dogModelList;
    }

    @Override
    public Fragment getItem(int position) {
        return DogViewFragment.newInstance(dogModelList.get(position));
    }

    @Override
    public int getCount() {
        return dogModelList.size();
    }

    public void addDogModel(DogModel dogModel) {

        if (!dogModelList.isEmpty() && !(dogModelList.size() == 2)) {
            dogModelList.remove(dogModelList.size() - 1);
        }
        dogModelList.add(dogModel);
        dogModelList.add(dogModel);
        for (DogModel tempDogModel : dogModelList) {
            Log.d(TAG, tempDogModel.getUrl());
        }
        notifyDataSetChanged();
    }

    public void removeDogModel(DogModel dogModel) {

        if (dogModelList.size() != 2 && dogModelList.size() != 4) {
            int idx = dogModelList.lastIndexOf(dogModel);
            DogModel tempDogModel = dogModelList.get(idx + 1);
            dogModelList.add(idx, tempDogModel);
        }
        dogModelList.remove(dogModel);
        dogModelList.remove(dogModel);
        notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}
