package com.meesho.viewpager.meesho.presenter;

import android.view.View;

import com.meesho.viewpager.meesho.MainMVP;
import com.meesho.viewpager.meesho.model.DogModel;
import com.meesho.viewpager.meesho.model.MainModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by brainnr on 10/23/17.
 */

public class MainPresenter implements MainMVP.RequiredPresenterOps, MainMVP.PresenterOps {

    private final String TAG = getClass().getSimpleName();

    private WeakReference<MainMVP.RequiredViewOps> requiredViewOps;
    private MainMVP.ModelOps modelOps;
    private boolean isChangingConfig;


    private List<DogModel> dogModelList;
    private int maxDogsNum;

    public MainPresenter(MainMVP.RequiredViewOps requiredViewOps) {
        this.requiredViewOps = new WeakReference<MainMVP.RequiredViewOps>(requiredViewOps);
        this.modelOps = new MainModel(this);
        dogModelList = new ArrayList<>();
        maxDogsNum = DogModel.dogList.length;
    }

    @Override
    public void dogAdded(DogModel dogModel) {
        dogModelList.add(dogModel);
        if (dogModelList.size() == maxDogsNum) {
            requiredViewOps.get().toggleAddBtn(View.GONE);
        }
        requiredViewOps.get().toggleDeleteBtn(View.VISIBLE);
        requiredViewOps.get().showToast("new Dog added at position: " + dogModel.getIdx());
        requiredViewOps.get().addViewPagerItem(dogModel);
        if (dogModelList.size() >= 2) {
            requiredViewOps.get().startAutoScroll();
        }
    }

    @Override
    public void dogRemoved(DogModel dogModel) {
        if (dogModelList.contains(dogModel)) {
            dogModelList.remove(dogModel);
        }

        if (dogModelList.isEmpty()) {
            requiredViewOps.get().toggleDeleteBtn(View.GONE);
        }
        requiredViewOps.get().toggleAddBtn(View.VISIBLE);
        requiredViewOps.get().removeViewPagerItem(dogModel);
        requiredViewOps.get().showToast("Dog removed");
        if (dogModelList.size() < 2) {
            requiredViewOps.get().stopAutoScroll();
        }
    }

    @Override
    public void addNewDog() {
        int idx = 0;
        if (!dogModelList.isEmpty()) {
            DogModel dogModel = dogModelList.get(dogModelList.size() - 1);
            idx = dogModel.getIdx() + 1;
            if (idx == maxDogsNum) {
                idx = 0;
            }
        }
        DogModel dogModel = new DogModel(idx);
        modelOps.insertDog(dogModel);
    }

    @Override
    public void deleteDogModel() {
        if (!dogModelList.isEmpty()) {
            modelOps.removeDog(dogModelList.get(0));
        }
    }

    @Override
    public void onConfigurationChanged(MainMVP.RequiredViewOps view) {
        requiredViewOps = new WeakReference<MainMVP.RequiredViewOps>(view);
    }

    @Override
    public void onDestroy(boolean isChangingConfig) {
        requiredViewOps = null;
        this.isChangingConfig = isChangingConfig;
        if (!isChangingConfig) {
            modelOps.onDestroy();
        }
    }
}
