package com.meesho.viewpager.meesho;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * Created by brainnr on 10/23/17.
 */

public class StateMaintainer {

    private final String TAG = getClass().getSimpleName();

    private final String stateMaintainerTag;
    private WeakReference<FragmentManager> fragmentManager;
    private StateMngFragment stateMngFragment;

    public StateMaintainer(FragmentManager fragmentManager, String stateMaintainerTag) {
        this.fragmentManager = new WeakReference<FragmentManager>(fragmentManager);
        this.stateMaintainerTag = stateMaintainerTag;
    }


    public boolean firstTimeIn() {
        try {
            stateMngFragment = (StateMngFragment) fragmentManager.get().findFragmentByTag(stateMaintainerTag);
            if (stateMngFragment == null) {
                stateMngFragment = new StateMngFragment();
                fragmentManager.get().beginTransaction().add(stateMngFragment, stateMaintainerTag).commitAllowingStateLoss();
                return true;
            } else {
                return false;
            }
        } catch (NullPointerException e) {
            return false;
        }
    }

    public void put(String key, Object object) {
        stateMngFragment.put(key, object);
    }

    public void put(Object object) {
        stateMngFragment.put(object.getClass().getName(), object);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        return (T) stateMngFragment.get(key);
    }

    public boolean hasKey(String key) {
        return stateMngFragment.get(key) != null;
    }

    public static class StateMngFragment extends Fragment {
        private HashMap<String, Object> data = new HashMap<>();

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        public void put(String key, Object object) {
            data.put(key, object);
        }

        public void put(Object object) {
            data.put(object.getClass().getName(), object);
        }

        @SuppressWarnings("unchecked")
        public <T> T get(String key) {
            return (T) data.get(key);
        }
    }
}
